#
# Build script for Arraylist
#

#-----------------------------------------------------------
# User-defined part start
#

# BIN_LIB is the destination library for the service program.
# the rpg modules and the binder source file are also created in BIN_LIB.
# binder source file and rpg module can be remove with the clean step (make clean)
BIN_LIB=QGPL

DEF=THREAD_SAFE

TGTRLS=*CURRENT

# RCFLAGS = RPG compile parameter
RCFLAGS=OPTION(*SRCSTMT) DBGVIEW(*LIST) DEFINE($(DEF)) OPTIMIZE(*BASIC) STGMDL(*INHERIT) TGTRLS($(TGTRLS))

# LFLAGS = binding parameter
LFLAGS=STGMDL(*INHERIT) TGTRLS($(TGTRLS))


#
# User-defined part end
#-----------------------------------------------------------


OBJECTS = arraylist
 
 
.SUFFIXES: .rpgle .c .cpp
 
# suffix rules
.rpgle:
	system -i "CHGATR OBJ('$<') ATR(*CCSID) VALUE(1252)"
	system "CRTRPGMOD $(BIN_LIB)/$@ SRCSTMF('$<') $(RCFLAGS)"
        
all: clean compile
 
arraylist:

compile: arraylist.bnd $(OBJECTS)
	system "CRTSRVPGM $(BIN_LIB)/ARRAYLIST MODULE($(BIN_LIB)/ARRAYLIST) $(LFLAGS) EXPORT(*SRCFILE) SRCFILE($(BIN_LIB)/ALISTSRV) TEXT('Arraylist')" 
	
arraylist.bnd: .PHONY
	-system "DLTF $(BIN_LIB)/ALISTSRV"
	system "CRTSRCPF $(BIN_LIB)/ALISTSRV RCDLEN(112)"
	system "CPYFRMIMPF FROMSTMF('$@') TOFILE($(BIN_LIB)/ALISTSRV ARRAYLIST) RCDDLM(*ALL) STRDLM(*NONE) RPLNULLVAL(*FLDDFT)"

clean:
	-system "DLTMOD $(BIN_LIB)/ARRAYLIST"
	-system "DLTF $(BIN_LIB)/ALISTSRV"

purge: clean
	-system "DLTSRVPGM $(BIN_LIB)/ARRAYLIST"

release: .PHONY
	-system "DLTOBJ $(BIN_LIB)/ARRAYLIST OBJTYPE(*FILE)"
	system "CRTSAVF $(BIN_LIB)/ARRAYLIST"
	system "CHGOWN OBJ('/QSYS.LIB/$(BIN_LIB).LIB/ARRAYLIST.SRVPGM') NEWOWN(QPGMR)"
	system "SAVOBJ OBJ(ARRAYLIST) LIB($(BIN_LIB)) DEV(*SAVF) OBJTYPE(*SRVPGM) SAVF($(BIN_LIB)/ARRAYLIST) TGTRLS($(TGTRLS)) DTACPR(*YES)"

.PHONY:
