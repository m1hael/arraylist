Changelog
=========

Release 2.0.2
-------------

- fixed memory leak in arraylist_dispose()

Release 2.0.1
-------------

- added exports swap and sublist
